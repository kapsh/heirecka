# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hoyon tag=${PV} ]

export_exlib_phases src_install

SUMMARY="MPRIS2 plugin for mpv written in C"
DESCRIPTION="
It implements the org.mpris.MediaPlayer2 and org.mpris.MediaPlayer2.Player
D-Bus interfaces.
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        media/mpv [[ note = [ checks mpv.pc ] ]]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.52] [[ note = [ 7a36255 ] ]]
"

DEFAULT_SRC_COMPILE_PARAMS=( PKG_CONFIG=${PKG_CONFIG} )

mpv-mpris_src_install() {
    default

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/lib/mpv
    edo cp mpris.so "${IMAGE}"/usr/$(exhost --target)/lib/mpv

    # This is far from optimal but mpv's C plugins are reusing the scripting
    # interface and mpv only searches for them in ~/.config/mpv/scripts.
    # But I don't want to install arch-specific filesi into ~, duplicated for
    # every user and mpv installs no system-wide config file on its own.
    insinto /etc/mpv
    hereins mpv.conf <<EOF
--script=/usr/host/lib/mpv/mpris.so
EOF
}

