# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'livestreamer.exlib', which is:
#     Copyright 2014 Benedikt Morbach and 2016 Julian Ospald

# Order matters unfortunately
if ever is_scm ; then
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
    require github [ tag=${PV} ]
else
    require github [ tag=${PV} ]
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
fi

SUMMARY="CLI for extracting streams from various websites to a video player"
DESCRIPTION="
Streamlink is a command-line utility that pipes video streams from various
services into a video player, such as VLC. The main purpose of Streamlink is
to allow the user to avoid buggy and CPU heavy flash plugins but still be able
to enjoy various streamed content. There is also an API available for
developers who want access to the video stream data.
Look at https://streamlink.github.io/plugin_matrix.html#plugin-matrix for a
list of plugins.
"
HOMEPAGE+=" https://streamlink.github.io/"

LICENCES="BSD-2 MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        (
            dev-python/iso3166[python_abis:*(-)?]
            dev-python/iso_639[python_abis:*(-)?]
        ) [[ *note = [ could use pycountry with STREAMLINK_USE_PYCOUNTRY ] ]]
        dev-python/isodate[python_abis:*(-)?]
        dev-python/pycryptodome[>=3.4.3&<4][python_abis:*(-)?]
        dev-python/PySocks[>=1.5.8][python_abis:*(-)?]
        dev-python/requests[>=2.21.0&<3.0][python_abis:*(-)?]
        dev-python/websocket-client[python_abis:*(-)?]
    test:
        dev-python/freezegun[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/pycountry[python_abis:*(-)?]
        dev-python/requests-mock[python_abis:*(-)?]
        dev-python/unittest2[python_abis:*(-)?]
    suggestion:
        media/ffmpeg [[
            description = [ Required for YouTube 1080p+ ]
        ]]
        media-video/rtmpdump[~scm] [[
            description = [ Required for playing RTMP based streams ]
        ]]
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}index.html#user-guide"
UPSTREAM_CHANGELOG="${HOMEPAGE}changelog.html"

