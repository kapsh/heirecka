From 920c3f2d300c40bc6da974a7b5e820639ed95438 Mon Sep 17 00:00:00 2001
From: Sebastian Meyer <mail@bastimeyer.de>
Date: Sat, 5 Sep 2020 08:19:10 +0200
Subject: [PATCH] plugins.sportschau: rewrite and fix plugin (#3142)

---
 src/streamlink/plugins/sportschau.py | 98 +++++++++++++++-------------
 tests/plugins/test_sportschau.py     |  8 +--
 2 files changed, 56 insertions(+), 50 deletions(-)

diff --git a/src/streamlink/plugins/sportschau.py b/src/streamlink/plugins/sportschau.py
index 1b6d1599..e7309433 100644
--- a/src/streamlink/plugins/sportschau.py
+++ b/src/streamlink/plugins/sportschau.py
@@ -1,46 +1,52 @@
-import re
-import json
-
-from streamlink.plugin import Plugin
-from streamlink.stream import HDSStream
-from streamlink.utils import update_scheme
-
-_url_re = re.compile(r"http(s)?://(\w+\.)?sportschau.de/")
-_player_js = re.compile(r"https?://deviceids-medp.wdr.de/ondemand/.*\.js")
-
-
-class sportschau(Plugin):
-    @classmethod
-    def can_handle_url(cls, url):
-        return _url_re.match(url)
-
-    def _get_streams(self):
-        res = self.session.http.get(self.url)
-        match = _player_js.search(res.text)
-        if match:
-            player_js = match.group(0)
-            self.logger.info("Found player js {0}", player_js)
-        else:
-            self.logger.info("Didn't find player js. Probably this page doesn't contain a video")
-            return
-
-        res = self.session.http.get(player_js)
-
-        jsonp_start = res.text.find('(') + 1
-        jsonp_end = res.text.rfind(')')
-
-        if jsonp_start <= 0 or jsonp_end <= 0:
-            self.logger.info("Couldn't extract json metadata from player.js: {0}", player_js)
-            return
-
-        json_s = res.text[jsonp_start:jsonp_end]
-
-        stream_metadata = json.loads(json_s)
-
-        hds_url = stream_metadata['mediaResource']['dflt']['videoURL']
-        hds_url = update_scheme(self.url, hds_url)
-
-        return HDSStream.parse_manifest(self.session, hds_url).items()
-
-
-__plugin__ = sportschau
+import logging
+import re
+
+from streamlink.plugin import Plugin
+from streamlink.plugin.api import validate
+from streamlink.stream import HLSStream
+from streamlink.utils import parse_json, update_scheme
+
+log = logging.getLogger(__name__)
+
+
+class Sportschau(Plugin):
+    _re_url = re.compile(r"https?://(?:\w+\.)*sportschau.de/")
+
+    _re_player = re.compile(r"https?:(//deviceids-medp.wdr.de/ondemand/\S+\.js)")
+    _re_json = re.compile(r"\$mediaObject.jsonpHelper.storeAndPlay\(({.+})\);?")
+
+    _schema_player = validate.Schema(
+        validate.transform(_re_player.search),
+        validate.any(None, validate.Schema(
+            validate.get(1),
+            validate.transform(lambda url: update_scheme("https:", url))
+        ))
+    )
+    _schema_json = validate.Schema(
+        validate.transform(_re_json.match),
+        validate.get(1),
+        validate.transform(parse_json),
+        validate.get("mediaResource"),
+        validate.get("dflt"),
+        validate.get("videoURL"),
+        validate.transform(lambda url: update_scheme("https:", url))
+    )
+
+    @classmethod
+    def can_handle_url(cls, url):
+        return cls._re_url.match(url) is not None
+
+    def _get_streams(self):
+        player_js = self.session.http.get(self.url, schema=self._schema_player)
+        if not player_js:
+            return
+
+        log.debug("Found player js {0}".format(player_js))
+
+        hls_url = self.session.http.get(player_js, schema=self._schema_json)
+
+        for stream in HLSStream.parse_variant_playlist(self.session, hls_url).items():
+            yield stream
+
+
+__plugin__ = Sportschau
diff --git a/tests/plugins/test_sportschau.py b/tests/plugins/test_sportschau.py
index e0ecbe39..37b008d1 100644
--- a/tests/plugins/test_sportschau.py
+++ b/tests/plugins/test_sportschau.py
@@ -1,20 +1,20 @@
 import unittest
 
-from streamlink.plugins.sportschau import sportschau
+from streamlink.plugins.sportschau import Sportschau
 
 
 class TestPluginSportschau(unittest.TestCase):
     def test_can_handle_url(self):
         should_match = [
             'http://www.sportschau.de/wintersport/videostream-livestream---wintersport-im-ersten-242.html',
-            'http://www.sportschau.de/weitere/allgemein/video-kite-surf-world-tour-100.html',
+            'https://www.sportschau.de/weitere/allgemein/video-kite-surf-world-tour-100.html',
         ]
         for url in should_match:
-            self.assertTrue(sportschau.can_handle_url(url))
+            self.assertTrue(Sportschau.can_handle_url(url))
 
     def test_can_handle_url_negative(self):
         should_not_match = [
             'https://example.com/index.html',
         ]
         for url in should_not_match:
-            self.assertFalse(sportschau.can_handle_url(url))
+            self.assertFalse(Sportschau.can_handle_url(url))
-- 
2.28.0

