# Copyright 2015, 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_SECONDARY_REPOSITORIES="content"
SCM_EXTERNAL_REFS="
    content:content
    thirdparty/Eigen:
    thirdparty/fmt:
    thirdparty/Spice:
"
SCM_content_REPOSITORY="https://github.com/CelestiaProject/CelestiaContent.git"

require github [ user=CelestiaProject ] cmake

SUMMARY="A 3D space simulation that lets you travel through the solar system"

DESCRIPTION="
Unlike most planetarium software, Celestia doesn't confine you to the surface
of the Earth. You can travel throughout the solar system, to any of over
100,000 stars, or even beyond the galaxy.
All movement in Celestia is seamless; the exponential zoom feature lets you
explore space across a huge range of scales, from galaxy clusters down to
spacecraft only a few meters across. A 'point-and-goto' interface makes it
simple to navigate through the universe to the object you want to visit.
Celestia is expandable. Celestia comes with a large catalog of stars,
galaxies, planets, moons, asteroids, comets, and spacecraft. If that's not
enough, you can download dozens of easy to install add-ons with more objects."

HOMEPAGE="http://www.shatters.net/celestia/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    glut   [[ description = [ Build a simple GLUT frontend ] ]]
    gtk2   [[ description = [ Build GUI with support for gnome instead of pure GTK ] ]]
    qt5    [[ description = [ Use cairo for the GTK splash screen ] ]]
    theora [[ description = [ Create Ogg/Theora videos ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( glut gtk2 qt5 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        sys-devel/gettext
    build+run:
        dev-lang/LuaJIT
        dev-libs/fmt[>=4.0.0]
        dev-libs/libepoxy
        media-libs/freetype:2
        media-libs/libpng:=
        sci-libs/eigen:3
        x11-dri/mesa
        glut? ( x11-dri/freeglut )
        gtk2? (
            dev-libs/glib:2
            x11-libs/gtk+:2
            x11-libs/gtkglext
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qt5? ( x11-libs/qtbase:5 )
        theora? ( media-libs/libtheora )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_CELX:BOOL=TRUE
    -DENABLE_DATA:BOOL=TRUE
    # Prefer libepoxy
    -DENABLE_GLEW:BOOL=FALSE
    -DENABLE_SPICE:BOOL=FALSE
    -DENABLE_TTF:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    GLUT
    'gtk2 GTK'
    'qt5 QT'
    THEORA
)

