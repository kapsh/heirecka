# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_install

SUMMARY="A music engraving program devoted to produce high quality sheet music"
DESCRIPTION="
It brings the aesthetics of traditionally engraved music to computer printouts."

HOMEPAGE="http://www.lilypond.org/"
DOWNLOADS="${HOMEPAGE}download/sources/v$(ever range -2)/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}doc/v$(ever range -2)/Documentation/web/manuals"

LICENCES="FDL-1.3 GPL-3 OFL-1.1 [[ note = [ font under mf/ ] ]]"
SLOT="0"
MYOPTIONS="debug doc"

DEPENDENCIES="
    build:
        dev-lang/perl:=
        dev-texlive/texlive-metapost
        media-gfx/fontforge[>=20110222]
        sys-apps/texinfo[>=6.1]
        sys-devel/bison[>=1.29]
        sys-devel/flex
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-text/dblatex[>=0.1.4]
            app-text/texi2html[>=1.82]
            media-gfx/ImageMagick
            media-libs/netpbm
            net-misc/rsync
        )
    build+run:
        app-text/ghostscript[>=8.60]
        app-text/texlive-core
        dev-lang/guile:2.0[>=2.0.7]
        dev-lang/python:2.7
        dev-libs/glib:2[>=2.38]
        dev-texlive/texlive-fontsrecommended [[ note = [ TeX Gyre fonts ] ]]
        media-libs/fontconfig[>=2.4.0]
        media-libs/freetype:2[>=2.1.10]
        x11-libs/pango[>=1.38.0]
"

# Tarball doesn't contain all the needed data for the tests (last checked 2.18.2)
# http://lilypond.org/doc/v2.18/input/regression/collated-files.html
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Needed to get the translations into the correct location
    --exec-prefix=/usr/$(exhost --target)
    --prefix=/usr
    --with-texgyre-dir=/usr/share/texmf-dist/fonts/opentype/public/tex-gyre
    --enable-guile2
    --disable-pipe
    --disable-profiling
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    'doc documentation'
)

DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" )

lilypond_src_install() {
    default

    # remove empty directory
    edo rmdir "${IMAGE}"/usr/share/man/{man1,}
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/${PN}/{${PV}/{python,},}

    # If someone is interested this could be made optional
    edo rm -r "${IMAGE}"/usr/share/emacs
}

